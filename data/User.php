<?php
/**
 * Created by PhpStorm.
 * User: jasonedstrom
 * Date: 3/23/14
 * Time: 12:37 AM
 */

namespace data;


class User {
    public $firstName, $lastName, $username, $comments;
    private $birthday, $password, $emailAddress;

    function __construct($birthday, $comments, $emailAddress, $firstName, $lastName, $password, $username)
    {
        $this->birthday = $birthday;
        $this->comments = $comments;
        $this->emailAddress = $emailAddress;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->password = $password;
        $this->username = $username;
    }
} 
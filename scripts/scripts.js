/**
 * Created by jasonedstrom on 2/3/14.
 */

(document).ready(function () {
jQuery.noConflict ();
})(jQuery);

(function($) {
    $(document).ready(function() {
        $('#product1').on('click', function() {
            new Messi('<img class="productimg" src="http://s.w.org/about/images/logos/wordpress-logo-notext-rgb.png"><p style="font-weight: bold; float:right;">Price: $150</p><p style="text-align: center;">Bring the beauty of the internet to you in all its glory.</p>', {title: 'Wordpress Theme', modal: true, buttons: [{id: 0, label: 'Close', val: 'X'}]});
        });

        $('#product2').on('click', function() {
            new Messi('<img class="productimg" src="http://prolificphoto.com/wp-content/uploads/2013/01/Adobe-Photoshop-Logo.png"><p style="font-weight: bold; float:right;">Price: $150</p><p style="text-align: center;">Bring the beauty of the internet to you in all its glory.</p>', {title: 'Photoshop Guru', modal: true, buttons: [{id: 0, label: 'Close', val: 'X'}]});
        });
    });
})(jQuery);

function formCheck()
{
    var z = true;
    var temp = new Array("FName","LName","Email","Birthday","Username","Password");
    for(var i in temp) {
        var value = document.getElementById(temp[i]).value;
        if(value == "" || value == "mm/dd/yyyy") {
            document.getElementById(temp).style.border="2px solid red";
            z = false;

        }else{
            if(verify(temp[i],value)){
                //document.getElementById(id).style.display="none";
                /*for(var i in tempArray) {
                    var value = document.getElementById(tempArray[i]).value;
                    if(value == ""){
                        z = false;
                        break;
                    }
                    else
                        z = true;
                }*/
                if (!z === false){
                z = true;
                }
            }
            else{
                z= false;
            }
        }
    }

    var checkbox = document.getElementById('TC').checked;


    if (!document.getElementById('TC').checked && z){
        z= false;
    }
    return z;
}

function fieldCheck(id, data)
{
    var temp = id, z = false;
    var tempArray = new Array("FName","LName","Email","Birthday","Username","Password");
    //id += "img";
    if(data == "" || value == "mm/dd/yyyy")
        document.getElementById(temp).style.border="2px solid red";
    else {
        if(verify(temp,data)){
            //document.getElementById(id).style.display="none";
            for(var i in tempArray) {
                var value = document.getElementById(tempArray[i]).value;
                if(value == ""){
                    z = false;
                    break;
                }
                else
                    z = true;
            }
        } else{
            z= false;
        }
    }


    return z;
}


function verify(temp, data)
{
    if(temp == "FName" || temp == "LName") {
        if(data.match(/^[A-Za-z\-]{2,25}$/)) {
            document.getElementById(temp).style.border="none";
            return true;
        }
        else {

            document.getElementById(temp).style.border="2px solid red";
            return false;
        }
    }

    else if(temp == "Email"){
        if(data.match(/^((\w+\+*\-*)+\.?)+@((\w+\+*\-*)+\.?)*[\w-]+\.[a-z]{2,6}$/i)){
            document.getElementById(temp).style.border="none";
            return true;
        }
        else
            document.getElementById(temp).style.border="2px solid red";
        return false;
    }
    else {
        document.getElementById(temp).style.border="none";
        return true;
    }
}